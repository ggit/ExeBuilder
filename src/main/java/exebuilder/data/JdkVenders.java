package exebuilder.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class JdkVenders {

  private static final Map<String, String> jdks = new HashMap<>();

  static {
    jdks.put("openjdk-14+36_windows-x64_bin.zip", "https://download.java.net/openjdk/jdk14/ri/openjdk-14+36_windows-x64_bin.zip");
    jdks.put("OpenJDK14U-jdk_x64_windows_hotspot_14_36.zip", "https://d2.injdk.cn/openjdk/adoptopenjdk/OpenJDK14U-jdk_x64_windows_hotspot_14_36.zip");
    jdks.put("zulu14.28.21-ca-jdk14.0.1-win_x64.zip", "https://cdn.azul.com/zulu/bin/zulu14.28.21-ca-jdk14.0.1-win_x64.zip");
    jdks.put("sapmachine-jdk-14_windows-x64_bin.zip", "https://d2.injdk.cn/openjdk/sapmachine/14/sapmachine-jdk-14_windows-x64_bin.zip");
    jdks.put("openjdk-15-ea+32_windows-x64_bin.zip","https://d2.injdk.cn/openjdk/openjdk/15ea/openjdk-15-ea+32_windows-x64_bin.zip");
  }


  public static String[] names() {
    String[] names = jdks.keySet().toArray(new String[0]);
    Arrays.sort(names);
    return names;
  }

  public static String getUrl(String name) {
    return jdks.get(name);
  }

  public static String getDefault() {
    return names()[0];
  }
}
